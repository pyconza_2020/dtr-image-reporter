import requests

DTR_URL = "http://dtr.local/api/v2.0"
PROJECTS = ["staging"]

def get_images(additional_projects=[], debug=False):
    projects = PROJECTS[:]
    projects.extend(additional_projects)
    out_dict = {}
    for project in projects:
        if debug:
            print(f'Getting list of images for "{project}" project')
        res = requests.get(f"{DTR_URL}/projects/{project}/repositories")
        out_dict[project] = [d["name"].split("/",1)[1] for d  in res.json()]
    return out_dict

if __name__ == "__main__":
    from argparse_helper.argparse_helper import CommandLineParser

    clp = CommandLineParser(
        "Tool to report on images in the Docker registry"
    )
    clp.add_quiet()
    clp.add_argument(
        "--include-prod", action="store_true", help="Also include prod images"
    )
    clp.parse()
    for (project, images) in get_images(["prod"] if clp.include_prod else [], debug=clp.include_output).items():
        print()
        print(f"{project}:")
        print(f"{'-'*len(project)}")
        if images:
            print("\n".join(images))
    print()